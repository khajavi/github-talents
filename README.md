# Github Talents
Github Talents list contributors of organization sorted by the number of their contributions.

## Building
run following command to create docker image file:

```bash
sbt docker:publishLocal
```

This command create corresponding docker image.

## Running the service

Run following command to run the service. 

```bash
docker run -p 8080:8080 -i -t io.khajavi.githubtalents/githubtalents:0.0.1-SNAPSHOT
```

Not that if want to handle Github's API rate limit restriction, set your Github Token as environment variable:

```bash
docker run --env GH_TOKEN="<your github token>" -p 8080:8080 -i -t io.khajavi.githubtalents/githubtalents:0.0.1-SNAPSHOT
```

Now you can get the list of top contributors to any organization:

```bash
curl -i "http://127.0.0.1:8080/org/{organization}/contributors"
```

An example request:

```bash
curl -i "http://127.0.0.1:8080/org/typelevel/contributors"
```

## Future Works
* [ ] Add Error Handing
* [ ] Add OpenAPI Specification
* [ ] Support native build