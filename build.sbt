val Http4sVersion = "0.21.13"
val CirceVersion = "0.13.0"
val Specs2Version = "4.10.5"
val LogbackVersion = "1.2.3"
val SttpVersion = "2.1.4"
val catsTaglessVersion = "0.11"
val PureConfigVersion = "0.12.3"
val RefinedVersion = "0.9.14"
val OdinVersion = "0.9.1"

lazy val root = (project in file("."))
  .settings(
    organization := "io.khajavi",
    name := "GithubTalents",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.13.3",
    libraryDependencies ++= Seq(
      "org.http4s"                   %% "http4s-blaze-server"           % Http4sVersion,
      "org.http4s"                   %% "http4s-blaze-client"           % Http4sVersion,
      "org.http4s"                   %% "http4s-circe"                  % Http4sVersion,
      "org.http4s"                   %% "http4s-dsl"                    % Http4sVersion,
      "io.circe"                     %% "circe-generic"                 % CirceVersion,
      "io.circe"                     %% "circe-parser"                  % CirceVersion,
      "org.specs2"                   %% "specs2-core"                   % Specs2Version % "test",
      "ch.qos.logback"               %  "logback-classic"               % LogbackVersion,
      "org.scalameta"                %% "svm-subs"                      % "20.2.0",
      "com.softwaremill.sttp.client" %% "async-http-client-backend-fs2" % SttpVersion,
      "com.softwaremill.sttp.client" %% "httpclient-backend-fs2"        % SttpVersion,
      "org.typelevel"                %% "cats-tagless-macros"           % catsTaglessVersion,
      "com.github.pureconfig"        %% "pureconfig"                    % PureConfigVersion,
      "com.github.pureconfig"        %% "pureconfig-cats-effect"        % PureConfigVersion,
      "eu.timepit"                   %% "refined"                       % RefinedVersion,
      "eu.timepit"                   %% "refined-pureconfig"            % RefinedVersion,
      "com.github.valskalla"         %% "odin-core"                     % OdinVersion
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3"),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    mainClass in Compile := Some(
      "io.khajavi.githubtalents.Main"
    ),
    dockerSettings
  )
  .enablePlugins(JavaServerAppPackaging)
  .enablePlugins(AssemblyPlugin)
  .enablePlugins(DockerPlugin)
  
val dockerSettings = Seq(
  dockerBaseImage := "openjdk:11-jre-slim",
  dockerExposedPorts := Seq(8080),
  dockerExposedVolumes := Seq("/opt/docker/logs"),
  daemonUserUid in Docker := Some("1001"),
  daemonUser in Docker := "github-talents",
  maintainer in Docker := "Milad Khajavi",
  dockerRepository := Some("io.khajavi.githubtalents")
)
