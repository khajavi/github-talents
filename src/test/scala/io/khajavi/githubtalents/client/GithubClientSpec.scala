package io.khajavi.githubtalents.client

import io.khajavi.githubtalents.data.{Contributor, Organization, Repository}

class GithubClientSpec extends org.specs2.mutable.Specification {
  val client = new FakeGithubRepositoryApi
  "GithubClient" should {
    "return list of repositories of given organization" in {
      val ORG = Organization("miras-tech")
      client.repositories(ORG).unsafeRunSync() must beEqualTo(
        List(
          Repository("miras-tech/MirasVoice"),
          Repository("miras-tech/MirasText"),
          Repository("miras-tech/sheldon-bot"),
          Repository("miras-tech/jspeech")
        )
      )
    }

    "GithubClient" should {
      "return list of contributions to a given repository" in {
        val REPO = Repository("miras-tech/MirasText")
        client.contributors(REPO).unsafeRunSync() must beEqualTo(
          List(
            Contributor("behnamsabeti", 3),
            Contributor("HosseinAbedi", 2),
            Contributor("RezaFahmi88", 1)
          )
        )
      }
    }
  }
}
