package io.khajavi.githubtalents.client

import cats.MonadError
import cats.effect.IO
import io.khajavi.githubtalents.client.api.RepositoryApi
import io.khajavi.githubtalents.data.{Contributor, Organization, Repository}

import scala.io.Source
import scala.util.Try

class FakeGithubRepositoryApi(implicit M: MonadError[IO, Throwable])
    extends RepositoryApi[IO] {

  private def read(file: String): IO[String] =
    M.fromEither(
      Try(
        Source
          .fromResource(file)
          .getLines()
          .toList
          .mkString("")
      ).toEither
    )

  override def repositories(org: Organization): IO[List[Repository]] =
    for {
      r <- read(s"github/orgs/${org.name}/repos.json")
      d <-
        io.circe.parser
          .decode[List[Repository]](r) match {
          case Left(value)  => M.raiseError(new Exception(value))
          case Right(value) => M.pure(value)
        }
    } yield (d)

  override def contributors(repo: Repository): IO[List[Contributor]] =
    for {
      r <- read(s"github/repos/${repo.name}/contributors.json")
      d <- r match {
        case "" => M.pure(List())
        case _ =>
          io.circe.parser
            .decode[List[Contributor]](r) match {
            case Left(value)  => M.raiseError(new Exception(value))
            case Right(value) => M.pure(value)
          }
      }
    } yield (d)
}
