package io.khajavi.githubtalents.rank

import cats.effect.{ContextShift, IO, Timer}
import io.khajavi.githubtalents.client.FakeGithubRepositoryApi
import io.khajavi.githubtalents.data.{Contributor, Organization}

class RankSpec extends org.specs2.mutable.Specification {
  import scala.concurrent.ExecutionContext.Implicits.global
  private implicit val contextShift: ContextShift[IO] = IO.contextShift(global)
  private implicit val timer: Timer[IO] = IO.timer(global)
  private val service: RankServiceImp[IO] = RankServiceImp(new FakeGithubRepositoryApi)
  
  "GithubClient" should {
    "return sorted list of contributors to a given organization" in {
      val ORG = Organization("miras-tech")
      service.topContributors(ORG).unsafeRunSync() must beEqualTo(
        List(
          Contributor("amir-vaheb", 7),
          Contributor("arjmandi", 4),
          Contributor("behnamsabeti", 3),
          Contributor("HosseinAbedi", 2),
          Contributor("RezaFahmi88", 1)
        )
      )
    }
  }
}
