package io.khajavi.githubtalents.data

import io.circe.{Decoder, HCursor}

case class Repository(name: String)
object Repository {
  implicit def repositoryDecoder: Decoder[Repository] =
    (c: HCursor) =>
      for {
        name <- c.downField("full_name").as[String]
      } yield Repository(name)
}
