package io.khajavi.githubtalents.data

import cats.implicits._
import io.circe.{HCursor, _}

case class Contributor(name: String, contributions: Int)

object Contributor {
  implicit def contributorDecoder: Decoder[Contributor] =
    (c: HCursor) =>
      for {
        n <- c.downField("login").as[String]
        c <- c.downField("contributions").as[Int]
      } yield Contributor(n, c)

  implicit val contributorOrdering: Ordering[Contributor] =
    Ordering.by(_.contributions)
}
