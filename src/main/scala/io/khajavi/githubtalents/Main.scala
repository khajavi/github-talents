package io.khajavi.githubtalents

import cats.effect.{ExitCode, IO, IOApp}
import fs2.Stream
import io.odin.{Logger, _}
import io.khajavi.githubtalents.client.SttpHttpClient
import io.khajavi.githubtalents.client.api.GithubClient
import io.khajavi.githubtalents.config.GithubTalentConfig
import io.khajavi.githubtalents.rank.RankServiceImp
import io.khajavi.githubtalents.routes.GithubTalentsEndpoint
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import sttp.client.asynchttpclient.fs2.AsyncHttpClientFs2Backend

import scala.concurrent.ExecutionContext

object Main extends IOApp {
  private val logger: Logger[IO] = consoleLogger[IO]()

  override def run(args: List[String]): IO[ExitCode] = {
    service.compile.drain.as(ExitCode.Success)
  }

  private val service = for {
    _ <- Stream.eval(logger.info("Starting Github Talent Service!"))
    be <- Stream.resource(
      AsyncHttpClientFs2Backend
        .resource[IO]()
    )
    sttpClient <- SttpHttpClient.stream(be)
    config <- GithubTalentConfig.stream[IO]
    gc = GithubClient[IO](sttpClient, config.githubApiConfig)
    rs = RankServiceImp[IO](gc)
    gr = GithubTalentsEndpoint(rs)
    sr <- BlazeServerBuilder[IO](ExecutionContext.global)
      .bindHttp(
        config.webserviceConfig.port.value,
        host = config.webserviceConfig.host.value
      )
      .withHttpApp(gr.routes.orNotFound)
      .serve
  } yield sr
  
}
