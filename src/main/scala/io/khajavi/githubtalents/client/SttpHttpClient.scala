package io.khajavi.githubtalents.client

import java.nio.ByteBuffer

import cats.effect.{Sync, Timer}
import cats.syntax.functor._
import fs2.Stream
import io.khajavi.githubtalents.client.SttpHttpClient.Backend
import io.khajavi.githubtalents.client.core.Http.Request
import io.khajavi.githubtalents.client.core.{Http, HttpClient, HttpMethod}
import sttp.client.asynchttpclient.WebSocketHandler
import sttp.client.{SttpBackend, basicRequest}
import sttp.model.{Header, Method, Uri}

final class SttpHttpClient[F[_]: Sync] private (
    be: Backend[F]
) extends HttpClient[F] {

  private def fromMethod(method: HttpMethod): Method =
    method match {
      case HttpMethod.GET    => Method.GET
      case HttpMethod.POST   => Method.POST
      case HttpMethod.DELETE => Method.DELETE
      case HttpMethod.PUT    => Method.PUT
    }

  import sttp.client.UriContext

  private def fromUrl(url: String, queryString: List[(String, String)]): Uri =
    uri"$url".params(queryString: _*)

  private def fromHeader(headers: Map[String, String]): Seq[Header] =
    headers.toSeq
      .map { case (k, v) => Header(k, v) }

  override def send(
      url: String,
      method: HttpMethod,
      request: Request
  ): F[Http.Response] = {
    val req = basicRequest
      .method(fromMethod(method), fromUrl(url, request.queryStrings))
      .headers(fromHeader(request.headers): _*)
      .body(request.body.getOrElse(""))

    val send = be.send(req).map { res =>
      val body = res.body.merge
      Http.Response(
        res.code.code,
        if (body.nonEmpty) Some(body) else None,
        res.headers.map(h => (h.name, h.value)).toMap
      )
    }
    send
  }
}

object SttpHttpClient {
  type Backend[F[_]] =
    SttpBackend[F, fs2.Stream[F, ByteBuffer], WebSocketHandler]

  def apply[F[_]: Sync: Timer](be: Backend[F]): F[SttpHttpClient[F]] =
    Sync[F].delay(new SttpHttpClient[F](be))
    
  def stream[F[_]: Sync: Timer](be: Backend[F]): fs2.Stream[F, SttpHttpClient[F]] =
    Stream.eval(SttpHttpClient(be))
}
