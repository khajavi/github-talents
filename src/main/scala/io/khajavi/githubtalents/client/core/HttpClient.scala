package io.khajavi.githubtalents.client.core

import io.khajavi.githubtalents.client.core.Http.{Request, Response}

trait HttpClient[F[_]] {
  def send(url: String, method: HttpMethod, request: Request): F[Response]
}
