package io.khajavi.githubtalents.client.core

import eu.timepit.refined.W
import eu.timepit.refined.api.Refined
import eu.timepit.refined.boolean.{And, Not}
import eu.timepit.refined.string.{EndsWith, StartsWith, Url}

object Http {
  type URL = String Refined And[Url, Not[EndsWith[W.`"/"`.T]]]
  type RelativeURLPredicate = StartsWith[W.`"/"`.T]
  type RelativeURL = String
  type Headers = Map[String, String]

  final case class Request(
      body: Option[String],
      headers: Headers,
      queryStrings: List[(String, String)]
  )

  final object Request {
    def withBody(body: Option[String]): Request =
      new Request(body, Map.empty, List.empty)
  }

  final case class Response(
      status: Int,
      body: Option[String],
      headers: Headers
  )

  final case class ResponseT[T](
      status: Int,
      body: T,
      headers: Headers
  )

}
