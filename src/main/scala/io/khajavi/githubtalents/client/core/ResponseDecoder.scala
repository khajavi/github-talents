package io.khajavi.githubtalents.client.core

import io.circe.Decoder
import io.khajavi.githubtalents.client.core.Http.{Response, ResponseT}
import io.khajavi.githubtalents.client.core.ResponseDecoder.Result

final class ResponseDecoder[A] private (val decode: Response => Result[A])

object ResponseDecoder {
  type Result[A] = Either[String, ResponseT[A]]

  def lift[T](f: Response => Result[T]): ResponseDecoder[T] =
    new ResponseDecoder(f)

  import cats.syntax.either._
  import io.circe.parser.parse

  def jsonOf[T: Decoder]: ResponseDecoder[T] =
    lift { response =>
      response.body
        .map { s =>
          parse(s)
            .flatMap(_.as[T])
            .leftMap(_.getMessage)
            .map(x => ResponseT[T](response.status, x, response.headers))
        }
        .getOrElse(Left("invalid json data!"))
    }
}
