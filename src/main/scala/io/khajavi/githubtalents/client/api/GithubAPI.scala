package io.khajavi.githubtalents.client.api

import io.khajavi.githubtalents.client.core.Http.RelativeURL
import io.khajavi.githubtalents.client.core.{HttpMethod, ResponseDecoder}
import io.khajavi.githubtalents.data.{Contributor, Organization, Repository}

final case class GithubAPI[O](
    url: RelativeURL,
    method: HttpMethod
)(implicit val responseDecoder: ResponseDecoder[O])

object GithubAPI {
  def ListOrgRepositories(
      org: Organization,
      page: Option[Int] = None
  ): GithubAPI[List[Repository]] = {
    GithubAPI(url = s"/orgs/${org.name}/repos${params(page)}", HttpMethod.GET)(
      responseDecoder = ResponseDecoder.jsonOf[List[Repository]]
    )
  }

  def ListRepositoryContributors(
      repo: Repository,
      page: Option[Int] = None
  ): GithubAPI[List[Contributor]] = {
    GithubAPI(
      url = s"/repos/${repo.name}/contributors${params(page)}",
      HttpMethod.GET
    )(
      responseDecoder = ResponseDecoder.jsonOf[List[Contributor]]
    )
  }

  private def params(page: Option[Int]): String =
    page match {
      case Some(value) => s"?page=$value"
      case None        => ""
    }
}
