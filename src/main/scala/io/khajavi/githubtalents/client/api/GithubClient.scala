package io.khajavi.githubtalents.client.api

import cats.MonadError
import cats.effect.{Sync, Timer}
import cats.implicits._
import io.odin.{Logger, consoleLogger}
import io.khajavi.githubtalents.client.api.GithubClient.nextPage
import io.khajavi.githubtalents.client.core.Http.{Request, ResponseT}
import io.khajavi.githubtalents.client.core.{Http, HttpClient, HttpMethod}
import io.khajavi.githubtalents.config.GithubApiConfig
import io.khajavi.githubtalents.data.{Contributor, Organization, Repository}

class GithubClient[F[_]: Sync: Timer](
    client: HttpClient[F],
    config: GithubApiConfig
)(implicit
    M: MonadError[F, Throwable]
) extends RepositoryApi[F] {
  private val logger: Logger[F] = consoleLogger()
  private val header = config.token match {
    case Some(token) =>
      Map(
        "Accept" -> "application/vnd.github.v3+json",
        "Authorization" -> s"token $token"
      )
    case None =>
      Map(
        "Accept" -> "application/vnd.github.v3+json"
      )
  }
  private val httpRequest = Http.Request(None, header, Nil)

  override def repositories(org: Organization): F[List[Repository]] =
    logger.info(
      s"Getting the list of ${org.name}'s repositories."
    ) *> requestAll(org)

  override def contributors(repo: Repository): F[List[Contributor]] =
    logger.info(
      s"Getting the list of ${repo.name}'s contributors"
    ) *> requestAll(repo)

  private def requestAll[I](
      req: I
  )(implicit GR: GithubApiBuilder[I]): F[List[GR.O]] = {
    Sync[F].tailRecM((1, List[GR.O]())) {
      case (page, acc) =>
        for {
          current <- request(GR.api(req, Some(page)), httpRequest)
          np = nextPage(current.headers.get("Link"))
          result = np match {
            case None        => Right(acc ++ current.body)
            case Some(value) => Left((value, acc ++ current.body))
          }
        } yield result
    }
  }

  private def request[O](
      cmd: GithubAPI[O],
      httpRequest: Request
  ): F[ResponseT[O]] = {
    val requestUrl = s"${config.baseUrl}${cmd.url}"
    logger.debug(s"Sending request to $requestUrl") *>
      client
        .send(
          requestUrl,
          HttpMethod.GET,
          httpRequest
        )
        .flatMap(res =>
          cmd.responseDecoder.decode(res) match {
            case Left(value)  => M.raiseError[ResponseT[O]](new Exception(value))
            case Right(value) => M.pure(value)
          }
        )
  }
}

object GithubClient {
  def apply[F[_]](client: HttpClient[F], config: GithubApiConfig)(implicit
      S: Sync[F],
      T: Timer[F],
      M: MonadError[F, Throwable]
  ): GithubClient[F] = new GithubClient(client, config)(S, T, M)

  private def nextPage(linkHeader: Option[String]): Option[Int] =
    for {
      link <- linkHeader
      nextPage <-
        link
          .split(",")
          .find(_.contains("rel=\"next\""))
          .map(
            _.split(";").head
              .replace("<", "")
              .replace(">", "")
              .trim
              .split("page=")
              .last
              .toInt
          )
    } yield nextPage
}
