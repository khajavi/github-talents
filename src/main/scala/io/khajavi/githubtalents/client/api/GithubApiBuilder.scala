package io.khajavi.githubtalents.client.api

import io.khajavi.githubtalents.data.{Contributor, Organization, Repository}

trait GithubApiBuilder[I] {
  type O
  def api: (I, Option[Int]) => GithubAPI[List[O]]
}

object GithubApiBuilder {
  implicit object listOrgRepositoriesInstance extends GithubApiBuilder[Organization] {
    type O = Repository
    def api: (Organization, Option[Int]) => GithubAPI[List[O]] =
      (org, page) => GithubAPI.ListOrgRepositories(org, page)
  }

  implicit object listRepositoryContributorsInstance extends GithubApiBuilder[Repository] {
    type O = Contributor
    def api: (Repository, Option[Int]) => GithubAPI[List[O]] =
      (org, page) => GithubAPI.ListRepositoryContributors(org, page)
  }
}
