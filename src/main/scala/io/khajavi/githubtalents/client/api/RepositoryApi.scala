package io.khajavi.githubtalents.client.api

import io.khajavi.githubtalents.data.{Contributor, Organization, Repository}

trait RepositoryApi[F[_]] {
  def repositories(org: Organization): F[List[Repository]]
  def contributors(repo: Repository): F[List[Contributor]]
}
