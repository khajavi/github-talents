package io.khajavi.githubtalents.config

import cats.effect.{Blocker, ContextShift, Sync}
import eu.timepit.refined.pureconfig._
import eu.timepit.refined.types.net.NonSystemPortNumber
import eu.timepit.refined.types.string.NonEmptyString
import io.khajavi.githubtalents.client.core.Http.URL
import pureconfig.generic.ProductHint
import pureconfig.generic.auto._
import pureconfig.module.catseffect.syntax._
import pureconfig.{CamelCase, ConfigFieldMapping, ConfigSource}

final case class GithubTalentConfig(
    githubApiConfig: GithubApiConfig,
    webserviceConfig: WebserviceConfig
)

final case class GithubApiConfig(
    baseUrl: URL,
    token: Option[NonEmptyString] = None
)

final case class WebserviceConfig(
    host: NonEmptyString,
    port: NonSystemPortNumber
)

object GithubTalentConfig {
  def load[F[_]: Sync: ContextShift](
      blocker: Blocker
  ): F[GithubTalentConfig] = {
    ConfigSource.default
      .at("github-talents")
      .loadF[F, GithubTalentConfig](blocker)
  }

  implicit def hint[T]: ProductHint[T] =
    ProductHint[T](ConfigFieldMapping(CamelCase, CamelCase))

  def stream[F[_]: Sync: ContextShift]: fs2.Stream[F, GithubTalentConfig] =
    fs2.Stream.eval(Blocker[F].use(load[F]))
}
