package io.khajavi.githubtalents.routes

import cats.effect.{Sync, Timer}
import cats.implicits._
import io.circe.generic.auto._
import io.circe.syntax._
import io.khajavi.githubtalents.data.Organization
import io.khajavi.githubtalents.rank.RankService
import io.odin.{Logger, consoleLogger}
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

class GithubTalentsEndpoint[F[_]: Sync: Timer](rankService: RankService[F]) {
  private val logger: Logger[F] = consoleLogger()
  private val dsl = Http4sDsl[F]
  import dsl._
  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "org" / orgName / "contributors" =>
        for {
          _ <- logger.info(s"New request for $orgName organization received!")
          top <- rankService.topContributors(Organization(orgName))
          res <- Ok(top.asJson)
        } yield res
    }
}

object GithubTalentsEndpoint {
  def apply[F[_]: Sync: Timer](
      rankService: RankService[F]
  ): GithubTalentsEndpoint[F] =
    new GithubTalentsEndpoint(rankService)
}
