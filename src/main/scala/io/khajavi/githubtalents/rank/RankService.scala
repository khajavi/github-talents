package io.khajavi.githubtalents.rank

import io.khajavi.githubtalents.data.{Contributor, Organization}

trait RankService[F[_]] {
  def topContributors(org: Organization): F[List[Contributor]]
}
