package io.khajavi.githubtalents.rank

import cats.Parallel
import cats.effect.{Sync, Timer}
import cats.implicits._
import io.odin.{Logger, consoleLogger}
import io.khajavi.githubtalents.client.api.RepositoryApi
import io.khajavi.githubtalents.data.{Contributor, Organization, Repository}

class RankServiceImp[F[_]: Sync: Parallel: Timer](client: RepositoryApi[F])
    extends RankService[F] {
  private val logger: Logger[F] = consoleLogger()
  override def topContributors(org: Organization): F[List[Contributor]] =
    for {
      r <- client.repositories(org)
      c <- contributors(r)
      _ <- logger.info("Computing the top contributors")
      s <- Sync[F].delay(descOrder(agg(c)))
    } yield s

  private def contributors(
      repositories: List[Repository]
  ): F[List[Contributor]] =
    for {
      r <- Sync[F].pure(repositories)
      c <- r.parTraverse(client.contributors).map(_.flatten)
    } yield c

  private def descOrder[T](list: List[T])(implicit ordering: Ordering[T]): List[T] =
    list.sorted(ordering.reverse)

  private def agg(list: List[Contributor]): List[Contributor] =
    list
      .groupBy(_.name)
      .map {
        case (name, list) => Contributor(name, list.map(_.contributions).sum)
      }
      .toList
}

object RankServiceImp {
  def apply[F[_]: Sync: Parallel: Timer](client: RepositoryApi[F]): RankServiceImp[F] =
    new RankServiceImp(client)
}
