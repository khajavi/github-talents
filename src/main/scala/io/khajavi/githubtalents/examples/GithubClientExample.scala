package io.khajavi.githubtalents.examples

import cats.effect.{ExitCode, IO, IOApp}
import fs2.Stream
import io.khajavi.githubtalents.client.SttpHttpClient
import io.khajavi.githubtalents.client.api.GithubClient
import io.khajavi.githubtalents.config.GithubTalentConfig
import io.khajavi.githubtalents.data.Organization
import io.khajavi.githubtalents.rank.RankServiceImp
import sttp.client.asynchttpclient.fs2.AsyncHttpClientFs2Backend

object GithubClientExample extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    stream.evalTap(putStrLn).compile.drain.as(ExitCode.Success)
  }

  private val stream = for {
    be <- Stream.resource(
      AsyncHttpClientFs2Backend
        .resource[IO]()
    )
    sttpClient <- SttpHttpClient.stream(be)
    config <- GithubTalentConfig.stream[IO]
    gc = (new GithubClient[IO](sttpClient, config.githubApiConfig))
    rs <- Stream.eval(IO(RankServiceImp[IO](gc)))
    cs <- Stream.eval(rs.topContributors(Organization("khajavionsultants")))
  } yield (cs)
  
  def putStrLn(input: Any): IO[Unit] = IO(println(input))
}
